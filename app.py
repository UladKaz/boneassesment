import streamlit as st
import numpy as np
import pywt
import cv2
import os
import matplotlib.pyplot as plt

@st.cache(persist=True)
def load_image(name):
    data_path = os.path.join('data', name)
    image = cv2.imread(data_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return image
    
@st.cache
def plot_image(image, wavelet, line, amplitudes, frequencies):
    time = np.arange(image.shape[1])
    fig = plt.figure(figsize=(10, 7))
    gs = fig.add_gridspec(2, 3)
    axes_signal = fig.add_subplot(gs[0, 0])
    axes_wavelet = fig.add_subplot(gs[:, 1:])
    axes_image = fig.add_subplot(gs[1, 0])
    axes_signal.plot(time, image[line])
    axes_wavelet.contourf(time, np.log2(1. / frequencies), amplitudes)
    axes_wavelet.set_xlabel('time')
    axes_wavelet.set_ylabel('log2 scale')
    axes_image.imshow(image)
    axes_image.plot(time, [line] * len(time), color='red')
    return fig
    

@st.cache
def get_image_names():
    folder = 'data'
    return [file for file in os.listdir(folder) if '.gitignore' not in file]

def main():
    image_list = get_image_names()
    image_name = st.sidebar.selectbox('Выберите картинку:', image_list)
    image = load_image(image_name)
    st.title('Обработка рентген снимка при помощи вейвлет-преобразований')
    st.sidebar.title('Параметры:')
    wavelets = pywt.wavelist(kind='continuous')
    wavelet = st.sidebar.selectbox('Выберите вейвлет:', wavelets)
    line = st.slider(label='Выберите строку', min_value=0, max_value=image.shape[0])
    scales = 2. ** np.arange(-3, 12)
    amplitudes, frequencies = pywt.cwt(image[line], scales, wavelet)
    fig = plot_image(image, wavelet, line, amplitudes, frequencies)
    st.pyplot(fig)


if __name__ == '__main__':
    main()
