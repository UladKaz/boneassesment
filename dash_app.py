import cv2
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import json
import numpy as np
import os
import plotly.graph_objs as go
import pywt

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

def load_image(filename):
    data_path = os.path.join('data', filename)
    image = cv2.imread(data_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image, not_usefull = pywt.dwt2(image, 'db6')
    return image

def set_app_layout(app, image):
    wavelets = pywt.wavelist(kind='continuous')
    scale = 2. ** np.arange(-3, 7)
    amplitudes, frequencies = pywt.cwt(image[image.shape[0] - 1], scale, 'mexh')
    app.layout = html.Div([
        html.Div([
            html.Div([
                html.H1('Анализ костного возраста с применением вейвлетов.')
            ]),
            html.Div([
                html.Label([
                    'Выберите вейвлет:',
                    dcc.Dropdown(
                        id='wavelet-options',
                        options=[{'label': wavelet, 'value': wavelet} for wavelet in wavelets],
                        value='mexh'
                    )
                ])
            ]),
            html.Div([
                dcc.Graph(
                    id='image',
                    figure={
                        'data': [
                            go.Heatmap(z=image, showscale=False, hoverinfo='none')
                        ],
                        'layout': go.Layout(
                            xaxis=dict(showgrid=False, zeroline=False, showspikes=True, spikethickness=1, spikedash='solid',
                                       spikecolor='white'),
                            yaxis=dict(autorange=False, range=(image.shape[0], 0),
                                        showgrid=False, zeroline=False, showspikes=True,
                                        spikemode='across', spikethickness=1, spikedash='solid', spikecolor='white')
                        )
                    },
                    config={'displayModeBar': False},
                    style={'height': '700px'}
                ),
                dcc.Graph(
                        id='wavelet',
                        figure={
                            'data': [
                                go.Contour(x=np.arange(image.shape[1]), y=1. / frequencies, z=amplitudes, contours=dict(showlines=False))
                            ]
                        }
                    )
                ], style={'display': 'flex', 'flex-direction': 'row'})
            ]),
    ])

if __name__ == '__main__':
    image = load_image('10005.png')
    set_app_layout(app, image)

    @app.callback(
        Output('wavelet', 'figure'),
        [Input('image', 'hoverData'),
         Input('wavelet-options', 'value')]
    )
    def update_wavelet_graph(hoverData, wavelet):
        if hoverData is not None:
            cursor_position = hoverData['points'][0]
        else:
            cursor_position = {'x': 0, 'y': 0}
        scale = 2. ** np.arange(-3, 7)
        amplitudes, frequencies = pywt.cwt(image[cursor_position['y']], scale, wavelet)
        wavelet_x = np.arange(image.shape[1])
        
        wavelet_plot = go.Contour(x=, y=np.log2(1. / frequencies), z=np.abs(amplitudes),
                                  contours=dict(showlines=False))
        line = go.Scatter(x=[cursor_position['x'], cursor_position['x']], y=[-1, 8], line=dict(color='white'))
        return {'data': [wavelet_plot, line]}
    app.run_server(debug=True)