# BoneAssesment

Bone assesment course project. We are using wavelets to try to decrease computations in CNN.

Для того, чтобы запустить:

- Скачать файлы изображений в папку data
- Установите библиотеки из requirements.txt
- Напишите streamlit run app.py
- В браузере перейдите во вкладку localhost:8501
